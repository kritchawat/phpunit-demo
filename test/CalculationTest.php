<?php

include __DIR__."/../src/Calculation.php";

use PHPUnit\Framework\TestCase;

class CalculationTest extends TestCase
{
    public function testCanPerformUnitTest()
    {
        $this->assertTrue(true);
    }

    public function testCanSumInteger()
    {
        $cal = $this->getMockBuilder('Calculation')
        ->setMethods(['checkType'])
        ->getMock();
        
        $cal->method('checkType')
        ->willReturn(true);

        $this->assertSame(5, $cal->sum(2,3));
    }

    public function testSumValidateFail()
    {
        $cal = $this->getMockBuilder('Calculation')
        ->setMethods(['checkType'])
        ->getMock();
        
        $cal->method('checkType')
        ->willReturn(false);

        $this->assertSame(5, $cal->sum(2,3));
    }

}